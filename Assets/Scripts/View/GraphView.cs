﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphView : MonoBehaviour
{
    [SerializeField] private GameObject nodeView;

    private GameObject[,] nodes;

    private MapData mapData;
    private Graph graph;

    private Pathfinder pathfinder;

    public GameObject[,] Nodes { get => nodes; }

    private void Start()
    {
        pathfinder = FindObjectOfType<Pathfinder>();
        mapData = FindObjectOfType<MapData>();
        graph = FindObjectOfType<Graph>();

        graph.InitializeWithMap(mapData);
        nodes = new GameObject[mapData.Width, mapData.Height];

        for (int y = 0; y < mapData.Height; y++)
            for (int x = 0; x < mapData.Width; x++)
            {
                GameObject newNode = Instantiate(nodeView);
                newNode.transform.parent = this.transform;
                NodeView newNodeNodeView = newNode.GetComponent<NodeView>();
                newNodeNodeView.Init(graph.Nodes[x,y]);
                newNodeNodeView.ColorNode(graph.Nodes[x,y].NodeType == NodeType.BLOCKED ? Color.black : Color.white);

                nodes[x, y] = newNode;
            }

        pathfinder.Init();
    }
}
