﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeView : MonoBehaviour
{
    [SerializeField] private GameObject tile;

    [Range(0, 0.5f)] public float margin = 0;

    public void Init(Node node)
    {
        if (tile != null)
        {
            gameObject.name = "Node (" + node.XIndex + ", " + node.YIndex + ")";
            gameObject.transform.position = node.Position;
            tile.transform.localScale = new Vector3(1 - margin, 1, 1 - margin);
        }
    }

    public void ColorNode(Color color)
    {
        Renderer tileRenderer = tile.GetComponent<Renderer>();

        if (tileRenderer != null)
            tileRenderer.material.color = color;
    }
}
