﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData : MonoBehaviour
{
    [SerializeField] private int width = 10;
    [SerializeField] private int height = 5;

    [SerializeField] private TextAsset mazeText;

    public int Width { get => width; }
    public int Height { get => height; }

    public int[,] CreateMap()
    {
        int[,] maze = new int[width, height];

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                maze[x, y] = 0;
            }
        }

        return maze;
    }

    public int[,] CreateMapWithRandomWalls()
    {
        int[,] maze = new int[width, height];

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (Random.Range(0, 100) > 29)
                    maze[x, y] = 0;
                else
                    maze[x, y] = 1;
            }
        }

        return maze;
    }

    public int[,] CreateMapFromFile()
    {
        if (mazeText == null) return null;

        List<string> rows = new List<string>();

        string textData = mazeText.text;
        string tmp = "";

        foreach (char c in textData)
        {
            if (c != '\n' && c != '\r')
                tmp += c.ToString();
            else if (tmp.Length > 0)
            {
                rows.Add(tmp);
                tmp = "";
            }
        }

        if (tmp != "")
            rows.Add(tmp);

        width = rows[0].Length;
        height = rows.Count;

        int[,] maze = new int[rows[0].Length, rows.Count];

        for (int y = 0; y < rows.Count; y++)
            for (int x = 0; x < rows[0].Length; x++)
                maze[x, y] = int.Parse(rows[rows.Count - 1 - y][x].ToString());

        return maze;
    }
}
