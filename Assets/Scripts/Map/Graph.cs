﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour
{
    [SerializeField] private Node[,] nodes;
    [SerializeField] private List<Node> walls = new List<Node>();

    private MapData mapData;

    public static readonly Vector2[] directions =
    {
        new Vector2(0, 1),
        new Vector2(1,1),
        new Vector2(1,0),
        new Vector2(0, -1),
        new Vector2(-1, -1),
        new Vector2(-1, 0),
        new Vector2(1, -1),
        new Vector2(-1, 1),
    };

    public Node[,] Nodes { get => nodes; }

    public void InitializeWithMap(MapData map)
    {
        mapData = map;
        int[,] maze = mapData.CreateMapFromFile();
        nodes = new Node[mapData.Width, mapData.Height];

        CreateNodes(maze);
        GetNodesNeighbors();
    }

    private void CreateNodes(int[,] maze)
    {
        for (int y = 0; y < mapData.Height; y++)
            for (int x = 0; x < mapData.Width; x++)
            {
                NodeType nodeType = (NodeType)maze[x, y];
                Node newNode = new Node(nodeType, x, y, new Vector3(x, 0, y));
                nodes[x, y] = newNode;
                if (nodeType == NodeType.BLOCKED)
                    walls.Add(newNode);
            }
    }

    private void GetNodesNeighbors()
    {
        for (int y = 0; y < mapData.Height; y++)
            for (int x = 0; x < mapData.Width; x++)
                if (nodes[x,y].NodeType != NodeType.BLOCKED) 
                    nodes[x, y].AddNeighbors(GetNeighbors(x, y));
    }

    public bool IsWithinBounds(int x, int y)
    {
        return (x >=0 && x < mapData.Width && y >= 0 && y < mapData.Height);
    }

    public List<Node> GetNeighbors (int x, int y)
    {
        if (nodes == null) return null;

        List<Node> neighborNodes = new List<Node>();

        foreach (Vector2 position in directions)
        {
            int newX = x + (int)position.x;
            int newY = y + (int)position.y;

            if (IsWithinBounds(newX, newY) && nodes[newX, newY] != null && nodes[newX, newY].NodeType != NodeType.BLOCKED)
                neighborNodes.Add(nodes[newX, newY]);
        }

        return neighborNodes;
    }
}
