﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField] private GameObject graph;

    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject playMenu;

    private Pathfinder pathfinder;

    private GameObject instantiatedGraph;

    private void Start()
    {
        pathfinder = FindObjectOfType<Pathfinder>();
    }

    public void StartPathfinding()
    {
        instantiatedGraph = Instantiate(graph, this.transform.position, this.transform.rotation);
        startMenu.SetActive(false);
        playMenu.SetActive(true);
    }

    public void RestartPathfinding()
    {
        DestroyImmediate(instantiatedGraph);
        pathfinder.Stop();
        instantiatedGraph = Instantiate(graph, this.transform.position, this.transform.rotation);
    }

    public void BackToMainMenu()
    {
        DestroyImmediate(instantiatedGraph);
        pathfinder.Stop();
        playMenu.SetActive(false);
        startMenu.SetActive(true);
    }

    public void ChangePathfindingAlgorithm(int value)
    {
        pathfinder.PathfindingTechnique = (PathfindingTechnique)value;
    }
}
