﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public enum PathfindingTechnique
{
    DJIKSTRA,
    OPTIMAL_A_STAR,
    FAST_A_STAR,
    PERLIN_A_STAR,
    RANDOM_A_STAR,
    BFS,
    DFS,
    GREEDY_BEST_FIRST_SEARCH,
}

[System.Serializable]
public enum PathfindingSearch
{
    DIJKSTRA,
    SIMPLE,
}


public class Pathfinder : MonoBehaviour
{
    [SerializeField] private PathfindingTechnique pathfindingTechnique;

    [SerializeField] private Vector2 startNodePosition;
    [SerializeField] private Vector2 endNodePosition;

    private Node startNode;
    private Node endNode;

    private MapData mapData;

    private GraphView graphView;
    private Graph graph;

    private int steps = 0;

    public PathfindingTechnique PathfindingTechnique { get => pathfindingTechnique; set => pathfindingTechnique = value; }
    public Vector2 StartNodePosition { get => startNodePosition; set => startNodePosition = value; }
    public Vector2 EndNodePosition { get => endNodePosition; set => endNodePosition = value; }

    public void Init()
    {
        graphView = FindObjectOfType<GraphView>();
        graph = FindObjectOfType<Graph>();
        mapData = FindObjectOfType<MapData>();

        if (startNodePosition.x < 0 || startNodePosition.x >= mapData.Width ||
            startNodePosition.y < 0 || startNodePosition.y >= mapData.Height)
            throw new Exception("Start Node wrongly setted");
        
        if (endNodePosition.x < 0 || endNodePosition.x >= mapData.Width ||
            endNodePosition.y < 0 || endNodePosition.y >= mapData.Height)
            throw new Exception("End Node wrongly setted");

        graphView.Nodes[(int)startNodePosition.x, (int)startNodePosition.y].GetComponent<NodeView>().ColorNode(Color.green);
        graphView.Nodes[(int)endNodePosition.x, (int)endNodePosition.y].GetComponent<NodeView>().ColorNode(Color.red);

        startNode = graph.Nodes[(int)startNodePosition.x, (int)startNodePosition.y];
        endNode = graph.Nodes[(int)endNodePosition.x, (int)endNodePosition.y];

        switch (pathfindingTechnique)
        {
            case PathfindingTechnique.DJIKSTRA:
                Dijkstra();
                break;
            case PathfindingTechnique.GREEDY_BEST_FIRST_SEARCH:
                GreedyBestFS();
                break;
            case PathfindingTechnique.OPTIMAL_A_STAR:
                AStar();
                break;
            case PathfindingTechnique.FAST_A_STAR:
                FastAStar();
                break;
            case PathfindingTechnique.PERLIN_A_STAR:
                PerlinAStar();
                break;
            case PathfindingTechnique.RANDOM_A_STAR:
                RandomAStar();
                break;
            case PathfindingTechnique.BFS:
                BFS();
                break;
            case PathfindingTechnique.DFS:
                DFS();
                break;
            default:
                break;
        }
    }

    private void AStar()
    {
        List<Node> queue = new List<Node>();
        int index = 0;

        StartCoroutine(RunAStarPathFinding(queue, index));
    }

    private void FastAStar()
    {
        List<Node> queue = new List<Node>();
        int index = 0;

        StartCoroutine(RunFastAStarPathFinding(queue, index));
    }

    private void PerlinAStar()
    {
        List<Node> queue = new List<Node>();
        int index = 0;

        StartCoroutine(RunPerlinNoiseAStarPathFinding(queue, index));
    }
    
    private void RandomAStar()
    {
        List<Node> queue = new List<Node>();
        int index = 0;

        StartCoroutine(RunRandomAStarPathFinding(queue, index));
    }

    private void Dijkstra()
    {
        List<Node> queue = new List<Node>();
        int index = 0;

        StartCoroutine(RunDijkstraPathFinding(queue, index));
    }

    private void GreedyBestFS()
    {
        List<Node> queue = new List<Node>();
        int index = 0;

        StartCoroutine(RunGreedyBestFSPathFinding(queue, index));
    }

    private void BFS()
    {
        List<Node> queue = new List<Node>();
        int index = 1;
        queue.Add(startNode);
        queue.AddRange(startNode.Neighbors);
        queue.ForEach((n) =>
        {
            if (n != startNode)
                graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.magenta);
        });

        StartCoroutine(RunBFSPathFinding(queue, index));
    }

    private void DFS()
    {
        List<Node> stack = new List<Node>();
        StartCoroutine(RunDFSPathFinding(startNode, stack));
    }

    private IEnumerator RunDFSPathFinding(Node node, List<Node> stack)
    {
        ++steps;
        if (node != startNode && node != endNode)
            graphView.Nodes[node.XIndex, node.YIndex].GetComponent<NodeView>().ColorNode(Color.cyan);

        yield return new WaitForSeconds(0.001f);

        stack.Add(node);

        if (node == endNode)
        {
            Debug.Log("Search steps: " + steps);
            GetSimplePath(node);
            throw new Exception("Stop search");
        }


        foreach (Node n in node.Neighbors)
        {
            if (!stack.Contains(n))
            {
                if (node != startNode)
                {
                    graphView.Nodes[node.XIndex, node.YIndex].GetComponent<NodeView>().ColorNode(Color.gray);
                    n.Previous = node;
                }
                yield return RunDFSPathFinding(n, stack);
            }
        }

    }

    private IEnumerator RunBFSPathFinding(List<Node> queue, int index)
    {
        Node poppedNode = null;

        while (index < queue.Count)
        {
            ++steps;
            poppedNode = queue[index];
            var neighbors = poppedNode.Neighbors.Where(x => !queue.Contains(x)).ToList();
            queue.ForEach((n) =>
            {
                if (n != startNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.gray);
            });
            queue.AddRange(neighbors);
            queue.ForEach((n) =>
            {
                if (neighbors.Contains(n) && n != endNode)
                {
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.magenta);
                    n.Previous = poppedNode;
                }
            });

            if (queue.Contains(endNode))
                break;

            ++index;
            yield return new WaitForSeconds(0.001f);
        }

        Node end = queue.Find(e => e == endNode);
        end.Previous = poppedNode;
        Debug.Log("Search steps: " + steps);

        GetSimplePath(end);
    }
    
    private IEnumerator RunDijkstraPathFinding(List<Node> queue, int index)
    {
        //Search
        startNode.Distance = 0.0f;
        queue.Add(startNode);

        Node poppedNode = null;

        while (index < queue.Count)
        {
            ++steps;
            poppedNode = queue[index];
            var neighbors = poppedNode.Neighbors;

            queue.ForEach((n) =>
            {
                if (n != startNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.gray);
            });

            neighbors.ForEach(n => 
            {
                if (queue.Contains(n) && queue[queue.IndexOf(n)].Distance > poppedNode.Distance + EuclideanDistance(n, poppedNode))
                {
                    n.Previous = poppedNode;
                    n.Distance = poppedNode.Distance + EuclideanDistance(n, poppedNode);
                    queue[queue.IndexOf(n)] = n;
                }
                else if (!queue.Contains(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = poppedNode.Distance + EuclideanDistance(n, poppedNode);
                    queue.Add(n);
                }

                if (n != startNode && n != endNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.magenta);
            });

            if (queue.Contains(endNode))
                break;

            ++index;
            yield return new WaitForSeconds(0.0000000001f);
        }

        Debug.Log("Search steps: " + steps);
        Node end = queue.Find(e => e == endNode);
        end.Previous = poppedNode;

        // Find
        GetSimplePath(end);
    }    

    private IEnumerator RunGreedyBestFSPathFinding(List<Node> queue, int index)
    {
        //Search & Find
        startNode.Distance = 0.0f;
        queue.Add(startNode);

        Node poppedNode = null;
        int start = -1;

        while (index < queue.Count)
        {
            ++steps;
            poppedNode = queue[index];
            var neighbors = poppedNode.Neighbors;

            neighbors.Add(poppedNode);
            neighbors.ForEach(n => n.Distance = DistanceFromEnd(n));
            var bestNode = neighbors.OrderBy(e => e.Distance).First();
            if (!queue.Contains(bestNode))
            {
                if (start != -1)
                {
                    GetSimplePath(queue[start], poppedNode);
                    graphView.Nodes[poppedNode.XIndex, poppedNode.YIndex].GetComponent<NodeView>().ColorNode(Color.blue);
                    start = -1;
                }

                index = queue.Count - 1;
                queue.Add(bestNode);
                if (bestNode != endNode)
                    graphView.Nodes[bestNode.XIndex, bestNode.YIndex].GetComponent<NodeView>().ColorNode(Color.blue);
            }
            else
            {
                if (start == -1)
                    start = index;
                neighbors.Where(n => n != bestNode && !queue.Contains(n)).ToList().ForEach(n => graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.gray));
                neighbors.Where(n => n != bestNode && !queue.Contains(n)).ToList().ForEach(n => n.Previous = poppedNode);
                queue.AddRange(neighbors.Where(n => n != bestNode && !queue.Contains(n)));
            }

            if (queue.Contains(endNode))
                break;

            ++index;
            yield return new WaitForSeconds(0.0000000001f);
        }

        Debug.Log("Search steps: " + steps);
    }

    private IEnumerator RunAStarPathFinding(List<Node> queue, int index)
    {
        //Search
        startNode.Distance = DistanceFromEnd(startNode);
        startNode.Priority = (int)startNode.Distance;
        queue.Add(startNode);

        Node poppedNode = null;

        while (index < queue.Count)
        {
            ++steps;
            poppedNode = queue[index];
            var neighbors = poppedNode.Neighbors;

            queue.ForEach((n) =>
            {
                if (n != startNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.gray);
            });

            neighbors.ForEach(n =>
            {
                if (queue.Contains(n) && queue[queue.IndexOf(n)].Distance > poppedNode.Distance + EuclideanDistance(n, poppedNode) + DistanceFromEnd(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = poppedNode.Distance + EuclideanDistance(n, poppedNode) + DistanceFromEnd(n);
                    queue[queue.IndexOf(n)] = n;
                }
                else if (!queue.Contains(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = poppedNode.Distance + EuclideanDistance(n, poppedNode) + DistanceFromEnd(n);
                    n.Priority = (int)n.Distance;
                }

                if (n != startNode && n != endNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.magenta);
            });

            queue.AddRange(neighbors.Where(n => !queue.Contains(n)));
            queue[index].Priority = 10000000;
            queue = queue.OrderBy(n => n.Priority).ToList();

            if (queue.Contains(endNode))
                break;

            yield return new WaitForSeconds(0.0000000001f);
        }

        Debug.Log("Search steps: " + steps);
        Node end = queue.Find(e => e == endNode);
        end.Previous = poppedNode;
        startNode.Previous = null;

        // Find
        GetSimplePath(end);
    }
    
    private IEnumerator RunFastAStarPathFinding(List<Node> queue, int index)
    {
        //Search
        startNode.Distance = DistanceFromEnd(startNode);
        startNode.Priority = (int)startNode.Distance;
        queue.Add(startNode);

        Node poppedNode = null;

        while (index < queue.Count)
        {
            ++steps;
            poppedNode = queue[index];
            var neighbors = poppedNode.Neighbors;

            queue.ForEach((n) =>
            {
                if (n != startNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.gray);
            });

            neighbors.ForEach(n =>
            {
                if (queue.Contains(n) && queue[queue.IndexOf(n)].Distance > DistanceFromStart(n) + DistanceFromEnd(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = DistanceFromStart(n) + DistanceFromEnd(n);
                    queue[queue.IndexOf(n)] = n;
                }
                else if (!queue.Contains(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = DistanceFromStart(n) + DistanceFromEnd(n);
                    n.Priority = (int)n.Distance;
                }

                if (n != startNode && n != endNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.magenta);
            });

            queue.AddRange(neighbors.Where(n => !queue.Contains(n)));
            queue[index].Priority = 10000000;
            queue = queue.OrderBy(n => n.Priority).ToList();

            if (queue.Contains(endNode))
                break;

            yield return new WaitForSeconds(0.0000000001f);
        }

        Debug.Log("Search steps: " + steps);
        Node end = queue.Find(e => e == endNode);
        end.Previous = poppedNode;
        startNode.Previous = null;

        // Find
        GetSimplePath(end);
    }

    private IEnumerator RunPerlinNoiseAStarPathFinding(List<Node> queue, int index)
    {
        //Search
        startNode.Distance = DistanceFromEnd(startNode);
        startNode.Priority = (int)startNode.Distance;
        queue.Add(startNode);

        Node poppedNode = null;

        while (index < queue.Count)
        {
            ++steps;
            poppedNode = queue[index];
            var neighbors = poppedNode.Neighbors;

            queue.ForEach((n) =>
            {
                if (n != startNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.gray);
            });

            neighbors.ForEach(n =>
            {
                float sinX = Mathf.Sin(n.XIndex);
                float sinY = Mathf.Sin(n.YIndex);
                float distance = Mathf.PerlinNoise(Mathf.Abs(sinX), Mathf.Abs(sinY));

                if (queue.Contains(n) && queue[queue.IndexOf(n)].Distance > distance)
                {
                    n.Previous = poppedNode;
                    n.Distance = distance;
                    queue[queue.IndexOf(n)] = n;
                }
                else if (!queue.Contains(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = distance;
                    n.Priority = DistanceFromStart(n) + DistanceFromEnd(n);
                }

                if (n != startNode && n != endNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.magenta);
            });

            queue.AddRange(neighbors.Where(n => !queue.Contains(n)));
            queue[index].Priority = 10000000;
            queue = queue.OrderBy(n => n.Priority).ToList();

            if (queue.Contains(endNode))
                break;

            yield return new WaitForSeconds(0.0000000001f);
        }

        Debug.Log("Search steps: " + steps);
        Node end = queue.Find(e => e == endNode);
        end.Previous = poppedNode;
        startNode.Previous = null;

        // Find
        GetSimplePath(end);
    }
    
    private IEnumerator RunRandomAStarPathFinding(List<Node> queue, int index)
    {
        //Search
        startNode.Distance = DistanceFromEnd(startNode);
        startNode.Priority = (int)startNode.Distance;
        queue.Add(startNode);

        Node poppedNode = null;

        while (index < queue.Count)
        {
            ++steps;
            poppedNode = queue[index];
            var neighbors = poppedNode.Neighbors;

            queue.ForEach((n) =>
            {
                if (n != startNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.gray);
            });

            neighbors.ForEach(n =>
            {
                if (queue.Contains(n) && queue[queue.IndexOf(n)].Distance > DistanceFromStart(n) + DistanceFromEnd(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = DistanceFromStart(n) + DistanceFromEnd(n);
                    queue[queue.IndexOf(n)] = n;
                }
                else if (!queue.Contains(n))
                {
                    n.Previous = poppedNode;
                    n.Distance = DistanceFromStart(n) + DistanceFromEnd(n);
                    n.Priority = UnityEngine.Random.Range(0.0f, 100.0f);
                }

                if (n != startNode && n != endNode)
                    graphView.Nodes[n.XIndex, n.YIndex].GetComponent<NodeView>().ColorNode(Color.magenta);
            });

            queue.AddRange(neighbors.Where(n => !queue.Contains(n)));
            queue[index].Priority = 10000000;
            queue = queue.OrderBy(n => n.Priority).ToList();

            if (queue.Contains(endNode))
                break;

            yield return new WaitForSeconds(0.0000000001f);
        }

        Debug.Log("Search steps: " + steps);
        Node end = queue.Find(e => e == endNode);
        end.Previous = poppedNode;
        startNode.Previous = null;

        // Find
        GetSimplePath(end);
    }

    private float EuclideanDistance(Node n, Node popped)
    {
        return (int)Mathf.Sqrt(Mathf.Pow(n.XIndex - popped.XIndex, 2) + Mathf.Pow(n.YIndex - popped.YIndex, 2));
    }
    
    private float DistanceFromEnd(Node n)
    {
        return (int)Mathf.Sqrt(Mathf.Pow(endNode.XIndex - n.XIndex, 2) + Mathf.Pow(endNode.YIndex - n.YIndex, 2));
    }

    private float DistanceFromStart(Node n)
    {
        return (int)Mathf.Sqrt(Mathf.Pow(n.XIndex - startNode.XIndex, 2) + Mathf.Pow(n.YIndex - startNode.YIndex, 2));
    }

    private void GetSimplePath(Node end)
    {
        steps = 0;
        while (end.Previous != null)
        {
            ++steps;
            end = end.Previous;
            if (end != startNode)
                graphView.Nodes[end.XIndex, end.YIndex].GetComponent<NodeView>().ColorNode(Color.blue);
        }

        Debug.Log("Path steps: " + steps);
    }

    private void GetSimplePath(Node start, Node end)
    {
        while (end.Previous != null)
        {
            ++steps;
            end = end.Previous;
            if (end != start)
                graphView.Nodes[end.XIndex, end.YIndex].GetComponent<NodeView>().ColorNode(Color.blue);
        }

        Debug.Log("Path steps: " + steps);
    }

    public void Stop()
    {
        StopAllCoroutines();
    }
}
