﻿using System.Collections.Generic;
using UnityEngine;

public class Node
{
    private NodeType nodeType;

    private int xIndex;
    private int yIndex;

    private Vector3 position;

    private List<Node> neighbors;

    private Node previous;

    private float distance;
    private float priority;

    public NodeType NodeType { get => nodeType; set => nodeType = value; }
    public int XIndex { get => xIndex; set => xIndex = value; }
    public int YIndex { get => yIndex; set => yIndex = value; }
    public Vector3 Position { get => position; }
    public List<Node> Neighbors { get => neighbors; }
    public Node Previous { get => previous; set => previous = value; }
    public float Distance { get => distance; set => distance = value; }
    public float Priority { get => priority; set => priority = value; }

    public Node(NodeType nodeType, int xIndex, int yIndex, Vector3 position, Node previous = null)
    {
        this.nodeType = nodeType;
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        this.position = position;
        neighbors = new List<Node>();
        this.previous = previous;
        distance = -100000;
    }

    public Node(Vector3 position, Node previous = null)
    {
        this.nodeType = NodeType.OPEN;
        xIndex = -1000;
        yIndex = -1000;
        this.position = position;
        neighbors = new List<Node>();
        this.previous = previous;
        distance = -1000000;
    }

    public void AddNeighbor(Node neighbor) => neighbors.Add(neighbor);

    public void AddNeighbors(List<Node> neighborsList) => neighbors.AddRange(neighborsList);

    public void Reset() => previous = null;
}
